package com.epam.tasks;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

/**
 * Created by ozhmak on 11.01.2019.
 */
public class TextSearchTest {

    @Test
    public void test() {
        String text = "Hello, this is test text. Use only for test purposes";
        assertArrayEquals(new int[] {15, 39}, TextSearch.find(text, "test"));
        assertArrayEquals(new int[] {}, TextSearch.find(text, "not found"));

        text = "";
        assertArrayEquals(new int[] {}, TextSearch.find(text, "test"));
    }
}
