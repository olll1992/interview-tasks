package com.epam.tasks;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by ozhmak on 10.01.2019.
 */
public class DetermineNumberTest {

    @Test
    public void test() {

        Assert.assertTrue(DetermineNumber.check("-1.2"));
        Assert.assertTrue(DetermineNumber.check("1.2001"));
        Assert.assertTrue(DetermineNumber.check("12"));
        Assert.assertTrue(DetermineNumber.check("-333"));
        Assert.assertTrue(DetermineNumber.check("9999999999999999999999999"));

        Assert.assertFalse(DetermineNumber.check("1.2.2"));
        Assert.assertFalse(DetermineNumber.check("1.2-0.2"));
        Assert.assertFalse(DetermineNumber.check("1x"));
        Assert.assertFalse(DetermineNumber.check(""));
        Assert.assertFalse(DetermineNumber.check("---"));
        Assert.assertFalse(DetermineNumber.check("x-1"));
    }
}
