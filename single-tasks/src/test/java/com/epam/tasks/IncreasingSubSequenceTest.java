package com.epam.tasks;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by ozhmak on 10.01.2019.
 */
public class IncreasingSubSequenceTest {

    @Test
    public void test() {

        Assert.assertEquals(6, IncreasingSubSequence.get(new int[] {1, 2, 7, 3, 4, 6, 7, 1, 2, 4, 6, 7, 9}));
        Assert.assertEquals(4, IncreasingSubSequence.get(new int[] {1, 2, 7, 3, 4, 6, 7, 1, 2, 9, 6, 7, 9}));

        Assert.assertEquals(0, IncreasingSubSequence.get(new int[] {}));
        Assert.assertEquals(1, IncreasingSubSequence.get(new int[] {1, 1, 1, 1}));
    }
}
