package com.epam.tasks;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by ozhmak on 10.01.2019.
 */
public class BracketsTest {

    @Test
    public void test() {

        Assert.assertTrue(Brackets.check("[[a]][h][[b][b][[c]]]"));
        Assert.assertTrue(Brackets.check(""));

        Assert.assertFalse(Brackets.check("[[a]][h][[b[][b][[c]]]"));
    }
}
