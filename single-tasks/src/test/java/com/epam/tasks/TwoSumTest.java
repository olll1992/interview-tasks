package com.epam.tasks;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by ozhmak on 10.01.2019.
 */
public class TwoSumTest {

    @Test
    public void test() {
        Assert.assertTrue(ArrayUtils.isEquals(new int[] {2, 7}, TwoSum.get(new int[] {2, 7, 11, 15}, 9)));
        Assert.assertTrue(ArrayUtils.isEquals(new int[] {2, 15}, TwoSum.get(new int[] {2, 7, 11, 15}, 17)));

        Assert.assertTrue(ArrayUtils.isEquals(new int[0], TwoSum.get(new int[] {2, 7, 11, 15}, 16)));
        Assert.assertTrue(ArrayUtils.isEquals(new int[0], TwoSum.get(new int[] {}, 0)));
    }
}
