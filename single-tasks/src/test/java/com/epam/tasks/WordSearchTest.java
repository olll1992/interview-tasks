package com.epam.tasks;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by ozhmak on 10.01.2019.
 */
public class WordSearchTest {

    private static final char[][] BOARD = {
            {'A','B','C','E'},
            {'S','F','C','S'},
            {'E','D','E','E'}
    };

    @Test
    public void test() {

        Assert.assertTrue(new WordSearch(BOARD, "ABCCFSE").exist());
        Assert.assertTrue(new WordSearch(BOARD, "ABCESCFSEDEE").exist());
        Assert.assertTrue(new WordSearch(BOARD, "").exist());
        Assert.assertTrue(new WordSearch(BOARD, "A").exist());

        Assert.assertFalse(new WordSearch(BOARD, "ABCCFSA").exist());
        Assert.assertFalse(new WordSearch(BOARD, "AA").exist());
        Assert.assertFalse(new WordSearch(BOARD, "CCEED").exist());


    }
}
