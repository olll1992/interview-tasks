package com.epam.tasks;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by ozhmak on 10.01.2019.
 */
public class ValidParenthesesTest {

    @Test
    public void test() {

        Assert.assertTrue(ValidParentheses.check("{}[]()"));
        Assert.assertTrue(ValidParentheses.check(""));
        Assert.assertTrue(ValidParentheses.check("1"));
        Assert.assertTrue(ValidParentheses.check("([{[]}{(1)}{[]}])"));

        Assert.assertFalse(ValidParentheses.check("{[(])}"));
        Assert.assertFalse(ValidParentheses.check("(}"));
        Assert.assertFalse(ValidParentheses.check("{)"));
    }
}
