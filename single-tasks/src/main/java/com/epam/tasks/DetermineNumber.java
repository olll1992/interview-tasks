package com.epam.tasks;

/**
 * Created by ozhmak on 09.01.2019.
 */
public class DetermineNumber {

    public static boolean check(String numberString) {
        if (numberString.length() == 0) {
            return false;
        }

        boolean valid = false;
        if (numberString.charAt(0) == '-') {
            valid = true;
        } else {
            for (char i = '0'; i <= '9'; i++) {
                if (numberString.charAt(0) == i) {
                    valid = true;
                    break;
                }
            }
        }
        if (!valid) {
            return false;
        }

        boolean dot = false;
        char c;
        for (int i = 1; i < numberString.length(); i++) {
            c = numberString.charAt(i);
            if (c < '0' || c > '9') {
                if (c == '.') {
                    if (dot) {
                        return false;
                    } else {
                        dot = true;
                    }
                } else {
                    return false;
                }
            }
        }
        return true;
    }
}
