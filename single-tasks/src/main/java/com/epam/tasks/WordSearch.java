package com.epam.tasks;

import org.apache.commons.lang3.ArrayUtils;

/**
 * Created by ozhmak on 07.01.2019.
 */
public class WordSearch {

    private final char[][] board;
    private final char[] characters;
    private final int[][] used;

    public WordSearch(char[][] board, String word) {
        this.board = ArrayUtils.clone(board);
        used = new int[board.length][];
        for (int i = 0; i < board.length; i++) {
            used[i] = new int[board[i].length];
        }
        characters = word.toCharArray();
    }

    public boolean exist() {
        int attempt = 1;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (checkCell(i, j, 0, attempt)) {
                    return true;
                }
                attempt++;
            }
        }
        return false;
    }

    private boolean checkCell(int i, int j, int charIndex, int attempt) {
        if (i < 0 || i >= board.length || j < 0 || j >= board[i].length) {
            return false;
        }

        if (charIndex >= characters.length) {
            return true;
        }

        if(board[i][j] == characters[charIndex] && used[i][j] != attempt) {
            setUsage(i, j, attempt);
            if (findNextNear(i, j, charIndex, attempt)) {
                return true;
            } else {
                rollbackUsage(i, j);
                return false;
            }
        }
        return false;
    }

    private boolean findNextNear(int i, int j, int charIndex, int attempt) {
        if (checkCell(i + 1, j, charIndex + 1, attempt)) {
            return true;
        }

        if (checkCell(i, j + 1, charIndex + 1, attempt)) {
            return true;
        }

        if (checkCell(i - 1, j, charIndex + 1, attempt)) {
            return true;
        }

        return checkCell(i, j - 1, charIndex + 1, attempt);
    }

    private void setUsage(int i, int j, int attempt) {
        used[i][j] = attempt;
    }

    private void rollbackUsage(int i, int j) {
        used[i][j] = 0;
    }
}
