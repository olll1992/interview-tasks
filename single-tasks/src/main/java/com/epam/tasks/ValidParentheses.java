package com.epam.tasks;

import java.util.LinkedList;

/**
 * Created by ozhmak on 09.01.2019.
 */
public class ValidParentheses {

    enum BracketTypes {
        brace,
        square_bracket,
        round_bracket
    }

    public static boolean check(String line) {
        LinkedList<BracketTypes> q = new LinkedList<>();
        for (int i = 0; i < line.length(); i++) {
            switch (line.charAt(i)) {
                case '{':
                    q.add(BracketTypes.brace);
                    break;
                case '[':
                    q.add(BracketTypes.square_bracket);
                    break;
                case '(':
                    q.add(BracketTypes.round_bracket);
                    break;
                case '}':
                    if (q.pollLast() != BracketTypes.brace) {
                        return false;
                    }
                    break;
                case ']':
                    if (q.pollLast() != BracketTypes.square_bracket) {
                        return false;
                    }
                    break;
                case ')':
                    if (q.pollLast() != BracketTypes.round_bracket) {
                        return false;
                    }
                    break;
                default:
                    break;
            }
        }
        return true;
    }
}
