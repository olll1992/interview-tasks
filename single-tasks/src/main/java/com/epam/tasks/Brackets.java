package com.epam.tasks;

/**
 * Created by ozhmak on 09.01.2019.
 */
public class Brackets {

    public static boolean check(String str) {
        int k = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '[') {
                k++;
            } else if (str.charAt(i) == ']') {
                k--;
            }
        }
        return k == 0;
    }
}
