package com.epam.tasks;

/**
 * Created by ozhmak on 07.01.2019.
 */
public class TwoSum {

    public static int[] get(int[] numbers, int target) {
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] <= target) {
                int secondTarget = target - numbers[i];
                for (int j = i; j < numbers.length; j++) {
                    if (numbers[j] == secondTarget) {
                        return new int[] {numbers[i], numbers[j]};
                    }
                }
            }
        }
        return new int[0];
    }
}
