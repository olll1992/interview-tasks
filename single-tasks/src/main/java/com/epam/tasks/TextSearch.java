package com.epam.tasks;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ozhmak on 11.01.2019.
 */
public class TextSearch {

    public static int[] find(String text, String word) {
        List<Integer> indexes = new ArrayList<>();
        int k = 0;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == word.charAt(k)) {
                k++;
                if (k == word.length()) {
                    indexes.add(i - (word.length() - 1));
                    k = 0;
                }
            } else {
                k = 0;
            }
        }
        int[] indexesArray = new int[indexes.size()];
        for (int j = 0; j < indexes.size(); j++) {
            indexesArray[j] = indexes.get(j);
        }
        return indexesArray;
    }
}
