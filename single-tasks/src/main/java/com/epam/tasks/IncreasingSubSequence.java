package com.epam.tasks;

/**
 * Created by ozhmak on 09.01.2019.
 */
public class IncreasingSubSequence {

    public static int get(int[] ints) {
        int longest = 0, current = 1;
        for (int i = 0; i < ints.length - 1; i++) {
            if (ints[i + 1] > ints[i]) {
                current++;
            } else {
                current = 1;
            }
            if (current > longest) {
                longest = current;
            }
        }
        return longest;
    }
}
