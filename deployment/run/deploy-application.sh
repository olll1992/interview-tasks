#!/usr/bin/env bash

PROJECT_FOLDER=$1
SERVICE_NAME=$2

SERVICE_DIR=/etc/systemd/system

name=$SERVICE_NAME
echo "$name"

sed -i 's,{JAVA_BIN},'"${JAVA_HOME}bin/java"',g' "${PROJECT_FOLDER}/${name}/bin/run-${name}.sh"
sed -i 's,{PROJECT_JAR},'"${PROJECT_FOLDER}/${name}/lib/$name.jar"',g' "${PROJECT_FOLDER}/${name}/bin/run-${name}.sh"

sudo systemctl stop "$name.service"
sudo chmod +w $SERVICE_DIR/"$name.service"

sudo mv $PROJECT_FOLDER/$name/system/* $SERVICE_DIR

sudo systemctl daemon-reload

sudo systemctl enable "$name.service"
sudo systemctl start "$name.service"
