##Jenkins:
- **URL**: http://jenkins.epam.ad4tech.net/
- **Login**: ozhmak
- **Password**: tochYn-gyqkem-dymke9

##Single-tasks:
- **Brackets** (Check right sequence of opened/closed square brackets)
- **DetermineNumber** (Determine that provided string is a number)
- **IncreasingSubSequence** (Get size of longest longest continuous increasing subsequence)
- **TextSearch** (Find indexes of string occurence in text)
- **TwoSum** (Find first two numbers in array, which sum equals to target)
- **ValidParentheses** (Check right sequence of opened/closed square brackets, circle brackets and parentheses)
- **WordSearch** (Check that provided string can be split by chars and can be found in square array of chars)

##Airport-code api:
- **URL**: http://airport-code.epam.ad4tech.net/api/airport/search/
- **Request header**: name=New York

##User-offers api:
- **URL**: http://user-offers.epam.ad4tech.net/api/user/offers/get?userId=123

##Prepared features
- JUnit tests
- Checkstyle plugin
- PMD plugin
- Findbugs plugin
- Push to develop/master restricted
- Merge pull requests only after 1 reviewer approve
- Auto build job after pull request merge
- Separate module deploy job
