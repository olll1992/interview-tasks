package com.epam.user.offers.configuration;

import com.epam.user.offers.service.UserOffersProvider;
import com.epam.user.offers.service.impl.UserOffersCsvProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by ozhmak on 09.01.2019.
 */
@Configuration
public class UserOffersAppConfiguration {

    @Bean
    public UserOffersProvider userOffersProvider(@Value("${user.offers.csv.path}") String filePath) {
        return new UserOffersCsvProvider(filePath);
    }
}
