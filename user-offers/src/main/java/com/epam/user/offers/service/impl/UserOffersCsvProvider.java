package com.epam.user.offers.service.impl;

import com.epam.user.offers.exception.EntityNotFoundException;
import com.epam.user.offers.service.UserOffersProvider;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ozhmak on 09.01.2019.
 */
public class UserOffersCsvProvider implements UserOffersProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserOffersCsvProvider.class);

    private Map<Integer, List<Integer>> cache = new HashMap<>();

    public UserOffersCsvProvider(String filePath) {
        Resource resource = new ClassPathResource(filePath);
        File file;
        try {
            file = resource.getFile();
        } catch (IOException e) {
            LOGGER.error("Cannot find file {}", filePath, e);
            //I know, it's not good to throw RuntimeException
            throw new RuntimeException("Cannot find file " + filePath, e);
        }

        if (!file.exists() || !file.isFile()) {
            LOGGER.error("Cannot find file {}", filePath);
            //I know, it's not good to throw RuntimeException
            throw new RuntimeException("Cannot find file " + filePath);
        }

        CsvParserSettings settings = new CsvParserSettings();
        settings.getFormat().setLineSeparator("\n");
        settings.getFormat().setDelimiter(',');
        settings.setMaxCharsPerColumn(20);

        CsvParser csvParser = new CsvParser(settings);
        csvParser.beginParsing(file);
        String[] nextLine;
        while ((nextLine = csvParser.parseNext()) != null) {
            try {
                Integer userId = Integer.parseInt(nextLine[0]);
                Integer offerId = Integer.parseInt(nextLine[1]);

                cache.computeIfAbsent(userId, k -> new ArrayList<>()).add(offerId);
            } catch (NumberFormatException e) {
                LOGGER.warn("File {}: Error processing line {}", filePath, nextLine);
            }
        }
        csvParser.stopParsing();
    }

    @Override
    public List<Integer> getUserOffers(Integer userId) {
        List<Integer> offerIds = cache.get(userId);
        if (offerIds == null) {
            throw new EntityNotFoundException("user", userId);
        }
        return offerIds;
    }
}
