package com.epam.user.offers.service;

import java.util.List;

/**
 * Created by ozhmak on 09.01.2019.
 */
public interface UserOffersProvider {

    List<Integer> getUserOffers(Integer userId);
}
