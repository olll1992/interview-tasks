package com.epam.user.offers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by ozhmak on 09.01.2019.
 */
@SpringBootApplication
public class UserOffersApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserOffersApplication.class, args);
    }
}
