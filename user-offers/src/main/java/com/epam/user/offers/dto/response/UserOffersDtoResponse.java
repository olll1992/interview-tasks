package com.epam.user.offers.dto.response;

import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Created by ozhmak on 09.01.2019.
 */
public class UserOffersDtoResponse {

    private final int total;
    private List<Integer> offerIds;

    public UserOffersDtoResponse(List<Integer> offerIds) {
        this.offerIds = offerIds;
        this.total = CollectionUtils.isEmpty(offerIds) ? 0 : offerIds.size();
    }

    public int getTotal() {
        return total;
    }

    public List<Integer> getOfferIds() {
        return offerIds;
    }
}
