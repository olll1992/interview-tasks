package com.epam.user.offers.exception;

/**
 * Created by ozhmak on 10.01.2019.
 */
public class EntityNotFoundException extends RuntimeException {
    private final String entityType;
    private final int id;

    public EntityNotFoundException(String entityType, int id) {
        super("Entity \"" + entityType + "\" with id=" + id + " not found");
        this.entityType = entityType;
        this.id = id;
    }

    public String getEntityType() {
        return entityType;
    }

    public int getId() {
        return id;
    }
}
