package com.epam.user.offers.configuration;

import com.epam.user.offers.dto.response.ErrorResponse;
import com.epam.user.offers.exception.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by ozhmak on 10.01.2019.
 */
@ControllerAdvice
public class ExceptionConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionConfiguration.class);

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<ErrorResponse> handleEntityNotFoundException(EntityNotFoundException ex) {
        LOGGER.error("Error while processing request", ex);

        return new ResponseEntity<>(new ErrorResponse(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
