package com.epam.user.offers.dto.response;

/**
 * Created by ozhmak on 10.01.2019.
 */
public class ErrorResponse {
    private String details;

    public ErrorResponse(String details) {
        this.details = details;
    }

    public String getDetails() {
        return details;
    }
}
