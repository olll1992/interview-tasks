package com.epam.user.offers.controller;

import com.epam.user.offers.dto.response.UserOffersDtoResponse;
import com.epam.user.offers.service.UserOffersProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * Created by ozhmak on 09.01.2019.
 */
@RestController
@RequestMapping("/api/user/offers")
public class UserOffersController {

    @Autowired
    private UserOffersProvider userOffersProvider;

    @GetMapping("/get")
    public UserOffersDtoResponse lookup(@NotEmpty @RequestParam("userId") Integer userId) {

        List<Integer> offerIds = userOffersProvider.getUserOffers(userId);

        return new UserOffersDtoResponse(offerIds);
    }
}
