package com.epam.user.offers.service;

import com.epam.user.offers.service.impl.UserOffersCsvProvider;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by ozhmak on 10.01.2019.
 */
public class UserOffersCsvProviderTest {

    @Test(expected = RuntimeException.class)
    public void testNotExistFile() {
        new UserOffersCsvProvider("user-offers.csv.invalid");
    }

    @Test
    public void testBadRows() {
        UserOffersCsvProvider userOffersCsvProvider = new UserOffersCsvProvider("bad-user-offers.csv");
        List<Integer> userOffers = userOffersCsvProvider.getUserOffers(1);

        assertEquals(2, userOffers.size());
    }
}
