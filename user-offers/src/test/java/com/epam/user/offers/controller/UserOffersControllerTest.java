package com.epam.user.offers.controller;

import com.epam.user.offers.dto.response.UserOffersDtoResponse;
import com.epam.user.offers.exception.EntityNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by ozhmak on 10.01.2019.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserOffersControllerTest {

    @Autowired
    private UserOffersController userOffersController;

    @Test
    public void testSuccess() {
        assertNotNull(userOffersController);

        UserOffersDtoResponse response = userOffersController.lookup(1);
        assertEquals(2, response.getTotal());
        assertEquals(2, response.getOfferIds().size());

        response = userOffersController.lookup(3);
        assertEquals(1, response.getTotal());
    }

    @Test
    public void testFail() {
        EntityNotFoundException ex = null;
        try {
            userOffersController.lookup(1000);
        } catch (EntityNotFoundException e) {
            ex = e;
            assertEquals("user", e.getEntityType());
            assertEquals(1000, e.getId());
        }
        assertNotNull(ex);
    }
}
