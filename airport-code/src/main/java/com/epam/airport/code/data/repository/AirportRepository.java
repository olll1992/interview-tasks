package com.epam.airport.code.data.repository;

import com.epam.airport.code.data.model.Airport;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by ozhmak on 09.01.2019.
 */
public interface AirportRepository extends JpaRepository<Airport, Long> {

    @Cacheable("airportCodes")
    List<Airport> findByNameContaining(String name);
}
