package com.epam.airport.code.dto.response;

import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Created by ozhmak on 09.01.2019.
 */
public class AirportCodeResponse {

    private final int total;
    private final List<AirportCodeEntity> codes;

    public AirportCodeResponse(List<AirportCodeEntity> codes) {
        this.codes = codes;
        this.total = CollectionUtils.isEmpty(codes) ? 0 : codes.size();
    }

    public int getTotal() {
        return total;
    }

    public List<AirportCodeEntity> getCodes() {
        return codes;
    }
}
