package com.epam.airport.code.dto.response;

/**
 * Created by ozhmak on 09.01.2019.
 */
public class AirportCodeEntity {
    private String name;
    private String code;

    public AirportCodeEntity(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }
}
