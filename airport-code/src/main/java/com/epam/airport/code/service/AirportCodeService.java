package com.epam.airport.code.service;

import com.epam.airport.code.data.model.Airport;
import com.epam.airport.code.data.repository.AirportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.List;

/**
 * Created by ozhmak on 10.01.2019.
 */
@Service
public class AirportCodeService {

    @Autowired
    private AirportRepository airportRepository;

    public List<Airport> searchByName(String name) {
        if (StringUtils.isEmpty(name)) {
            return Collections.emptyList();
        }
        return airportRepository.findByNameContaining(name);
    }
}
