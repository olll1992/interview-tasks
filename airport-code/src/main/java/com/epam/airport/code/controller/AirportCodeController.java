package com.epam.airport.code.controller;

import com.epam.airport.code.data.model.Airport;
import com.epam.airport.code.dto.response.AirportCodeEntity;
import com.epam.airport.code.dto.response.AirportCodeResponse;
import com.epam.airport.code.service.AirportCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ozhmak on 09.01.2019.
 */
@RestController
@RequestMapping("/api/airport")
public class AirportCodeController {

    @Autowired
    private AirportCodeService airportCodeService;

    @GetMapping("/search")
    public AirportCodeResponse lookup(@NotEmpty @RequestHeader("name") String name) {

        List<Airport> airports = airportCodeService.searchByName(name);

        List<AirportCodeEntity> airportCodes = airports.stream()
                .map(a -> new AirportCodeEntity(a.getName(), a.getCode()))
                .collect(Collectors.toList());

        return new AirportCodeResponse(airportCodes);
    }
}
