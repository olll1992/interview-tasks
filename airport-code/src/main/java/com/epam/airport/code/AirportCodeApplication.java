package com.epam.airport.code;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by ozhmak on 09.01.2019.
 */
@SpringBootApplication
public class AirportCodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(AirportCodeApplication.class, args);
    }
}
