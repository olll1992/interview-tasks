package com.epam.airport.code.controller;

import com.epam.airport.code.data.model.Airport;
import com.epam.airport.code.data.repository.AirportRepository;
import com.epam.airport.code.dto.response.AirportCodeResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by ozhmak on 10.01.2019.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AirportCodeControllerTest {

    @Autowired
    private AirportRepository airportRepository;

    @Autowired
    private AirportCodeController airportCodeController;

    @Before
    public void before() {
        Airport airport1 = new Airport();
        airport1.setId(1L);
        airport1.setName("Test Airport 1");
        airport1.setCode("TA1");
        airportRepository.save(airport1);

        Airport airport2 = new Airport();
        airport2.setId(2L);
        airport2.setName("Test Airport 2");
        airport2.setCode("TA2");
        airportRepository.save(airport2);
    }

    @Test
    public void testController() {
        assertNotNull(airportCodeController);

        AirportCodeResponse response = airportCodeController.lookup("Test");
        assertEquals(2, response.getTotal());
        assertEquals(2, response.getCodes().size());

        response = airportCodeController.lookup("Test Airport 1");
        assertEquals(1, response.getTotal());
        assertEquals(1, response.getCodes().size());

        response = airportCodeController.lookup("Unknown");
        assertEquals(0, response.getTotal());
        assertEquals(0, response.getCodes().size());

        response = airportCodeController.lookup("");
        assertEquals(0, response.getTotal());
        assertEquals(0, response.getCodes().size());
    }
}
